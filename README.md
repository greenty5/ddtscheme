# ddtScheme

OpenFOAM v8, validation and comparison between ddtSchemes. 

- runCase.py needs an argument:

   - 1 : pre-processing and run scalarTransportFoam using NDX(number of divisions), UU(velocity), DDT_SCHEMES variables.

   - 2 : post-processing (generattes T profiles on the center line of the flow region.
