import sys, os, random
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

'''
    args[1] = run scalarTransportFoam
    args[2] = run postProcessing
'''
args = sys.argv

sw = 1
DDT_SCHEME = "Euler" # ddt scheme setting
#DDT_SCHEME = "backward" # ddt scheme setting
#DDT_SCHEME = "CrankNicolson 0.9"
NDX = 100   # number of division
UU = 10  # velocity [m/s]

Courant = UU*0.0001/(1/NDX)

def preProcess(DDT_SCHEME, UU, NDX):
    os.system("foamListTimes -rm")
    os.system("rm -r postProcessing")
    os.system("rm -r 0/U")

    #os.system("pwd")
    with open("system/fvSchemes_org") as inFile:
        with open("system/fvSchemes", "wt") as outFile:
            for line in inFile:
                line = line.replace("DDT_SCHEME", DDT_SCHEME)
                outFile.write(line)

    with open("0/U_org") as inFile:
        with open("0/U", "wt") as outFile:
            for line in inFile:
                line = line.replace("UU", str(UU))
                outFile.write(line)

    with open("system/blockMeshDict_org") as inFile:
        with open("system/blockMeshDict", "wt") as outFile:
            for line in inFile:
                line = line.replace("NDX", str(NDX))
                outFile.write(line)

    os.system("blockMesh")

def run(DDT_SCHEME):
    os.system("foamJob scalarTransportFoam")
    #postProcess(DDT_SCHEME)

def postProcess(DDT_SCHEME):
    os.system("postProcess -func internalProbes")
    os.chdir("./postProcessing/internalProbes/")
    os.rename("0.04", "../../"+DDT_SCHEME)
    os.chdir("../../")
    os.rename(DDT_SCHEME+"/points_T.xy", DDT_SCHEME+".txt")
    os.system("rm -r {}".format(DDT_SCHEME))


if (args[1] == "1"):
    print("run scalarTransportFoam ... \n")
    preProcess(DDT_SCHEME, UU, NDX)
    run(DDT_SCHEME)
if (args[1] == "2"):
    print("postProcessing ... \n")
    postProcess(DDT_SCHEME)

print("+*+*+*+*+*+\n")
print("Courant number is ... {}\n".format(Courant))
print("+*+*+*+*+*+\n")