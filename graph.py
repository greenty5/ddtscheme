import numpy as np
import matplotlib as mpl
#import vtk
import tkinter
import seaborn as sns;# sns.set()
import matplotlib.pyplot as plt
import pandas as pd
from scipy.fftpack import fft, fftfreq

plt.rcParams['font.size'] = 18
plt.rcParams['font.family']= 'sans-serif'
#plt.rcparams['font.family']= 'times new roman'
plt.rcParams['font.sans-serif'] = ['Avenir next condensed']


def graph1(data1, data2, xAxis, yAxis):

    ####################
    #1つ目のウィンドウ
    ####################
    fig1 = plt.figure(figsize=(8,8))

    ################################
    #1つ目のウィンドウ 1つ目のグラフ
    ################################
    ax1 = fig1.add_subplot(211)
    ax1.plot(data1, data2, label='')
#    ax1.plot(data1.X, data1.Y2*1.0e6, label='Y2')
    ax1.grid(True)
#    ax1.set_xlim( (-0.0005,0.0065) )
#    ax1.set_ylim( (50,160) )
    ax1.set_xlabel(xAxis)
    ax1.set_ylabel(yAxis)
#    ax1.set_xlabel('dP [Pa]')
#    ax1.legend(["LR", "OUT"], fontsize=12)
#    ax1.set_title('200Hz', fontsize=12)

    fig1.tight_layout()  # グラフの文字がかぶらないようにする
    plt.show()

#inputFile = pd.read_csv('/Users/taka/hoge/U.csv',header=4, sep='\s+',names=('time','U'))
EULER= pd.read_csv('Euler.txt',header=0, sep='\s+',names=('X','Y','Z','T'))
BACKWARD = pd.read_csv('backward.txt',header=0, sep='\s+',names=('X','Y','Z','T'))

graph1(EULER.X, EULER.T, 'X [-]', 'T [-]')
